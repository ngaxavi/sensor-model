const http = require('http');
const express = require('express');
const cors = require('cors')
const morgan = require('morgan');
const jsonMiddleware = require('json-middleware');
const socketio = require('socket.io');
const request = require('superagent');

const Sensors = require('./sensor');
const Producer = require('./producer');
const logger = require('./logger');

const app = express();
const port = process.env.PORT || 3006;

app.use(cors());
app.use(morgan('combined'));
app.use(jsonMiddleware.middleware());

const kafka = new Producer();

const server = http.Server(app);
const io = socketio(server);

const sensors = new Sensors((data) => {
  io.emit(data.id, data);
});

// Add default sensors
sensors.addSensor('sensor_1');
sensors.addSensor('sensor_2');
sensors.addSensor('sensor_3', { initial: 24, min: 18, max: 28, updateValue: 0.002 });
sensors.addSensor('sensor_4', { initial: 22, min: 17, max: 26, updateValue: 0.003 });

app.get('/api/', (req, res) => {
  res.send('Hello from sensors world');
});

app.get('/api/sensor', (req, res) => {
  res.send(sensors.getSensorsIds());
});

app.get('/api/sensor/:sensor_id', (req, res) => {

  const sensorId = req.params.sensor_id;
  const data = sensors.getSensorJson(req.params.sensor_id);
  if (data !== null) {
    logger.info(data);
    if (sensorId === 'sensor_1') {
      kafka.sendMessage('sensor_data_1', data);
    } else {
      kafka.sendMessage('sensor_data_2', data);
    }
    res.send(data);
  }
  else
    res.sendStatus(404);
});



io.on('connection', (socket) => {
  logger.info('a user connected');

  socket.on('disconnect', () => {
    logger.info('user disconnected');
    sensors.stopListenSensorAll(socket.id);
  });

  socket.on('listen', (msg) => {
    logger.info(`listen event ${msg}`);
    var found = sensors.listenSensor(socket.id, msg);
    if (found) 
      socket.emit('listen', msg);
  });

  socket.on('stop', (msg) => {
    var found = sensors.stopListenSensor(socket.id, msg);
    if (found)
      socket.emit('stop', msg);
  });
});

server.listen(port, () => {
  logger.info(`Sensor Server started on port ${server.address().port}`);

  // TODO: Create a service that calls this api.
  setInterval(() => {
    request
        .get('http://localhost:3006/api/sensor/sensor_1')
        .end((err, resp) => {
            // console.log(resp.body);
            // resp.body
        });
  }, 15000);

  setInterval(() => {
    request
        .get('http://localhost:3006/api/sensor/sensor_3')
        .end((err, resp) => {
            // console.log(resp.body);
            // resp.body
        });
  }, 10000);
});
