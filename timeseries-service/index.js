process.stdin.resume(); // keep process alive

const Kafka = require('kafka-node');
const request = require('superagent');

const logger = require('./logger');

const consumerOptions = {
  host: 'zookeeper:2181',
  groupId: 'ts-group',
  sessionTimeout: 15000,
  protocol: ['roundrobin'],
  fromOffset: 'earliest' // equivalent of auto.offset.reset valid values are 'none', 'latest', 'earliest'
};


const consumer = new Kafka.ConsumerGroup(consumerOptions, ['sensor_data_filtered_1', 'sensor_data_filtered_2']);

// request.post('http://localhost:3000/api/tsm/5b2b8710d36f5e241a52953b')
//     .send({"1529603117834":24.055268006650284})
//     .set('Accept', 'application/json')
//     .end((err, res) => {

//       logger.info('New data are added in Time Series' +  res);
//     });


consumer.on('message', (metadata) => {

  // Output the actual message contents
  const dataValue = JSON.parse(metadata.value.toString());
  logger.info('data value', dataValue);


    logger.info(`send Data ${metadata.value.toString()}`);

    if (dataValue.id === 'sensor_1') {
      delete dataValue.id;
      request.post('http://10.0.115.212:3000/api/tsm/5b21303f6f4572682a077bb7')
      .send(dataValue)
      .set('Accept', 'application/json')
      .end((err, res) => {
  
        logger.info('New data are added From Sensor1');
      });
    } else  {
      delete dataValue.id;
      request.post('http://10.0.115.212:3000/api/tsm/5b2105ca4ac2f040904bfc07')
      .send(dataValue)
      .set('Accept', 'application/json')
      .end((err, res) => {
  
        logger.info('New data are added in Time Series');
      });
    }

   

});

//logging all errors
consumer.on('error', (err) => {
  logger.error('Error from consumer ' + err +  'code: ' + err.code);
});