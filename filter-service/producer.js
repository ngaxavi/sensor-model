
const Kafka = require('kafka-node');
const Promise = require('bluebird');

const client = new Kafka.Client('zookeeper:2181');

let producer;
let producerReady;

const logger = require('./logger');

const closeProducer = (cb) => {
  producer.close(cb);
};

const bindListeners = () => {
  
  producer.on('SIGTERM', () => {
    closeProducer((cb) => {});
  });

  producerReady = new Promise((resolve, reject) => {
    producer.on('ready', () => {
      logger.info('producer ready');
      resolve(producer);
    });
    producer.on('error', err => {
      logger.error(err);
      closeProducer();
      producer.connect();
      reject(err);
    });
  });
};

const initializeProducer = () => {
  producer = new Kafka.HighLevelProducer(client, {
    'requireAcks': 1
  });

  bindListeners();
};

class KafkaService {
    constructor() {
        initializeProducer();
    }

    sendMessage(topic, message, partition=0) {
      return producerReady.then(producer => {
      const msg = Buffer.from(JSON.stringify(message));

      const payload = [{ topic: topic, messages: msg, partition: partition}];
      producer.send(payload, (err, data) => {});
    })
    .catch(error => logger.error(`unable to send message ${error}`));
    }
}

module.exports = KafkaService;