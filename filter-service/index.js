process.stdin.resume(); // keep process alive

const Kafka = require('kafka-node');

const logger = require('./logger');

const Producer = require('./producer');

const consumerOptions = {
  host: 'zookeeper:2181',
  groupId: 'filter-group',
  sessionTimeout: 15000,
  protocol: ['roundrobin'],
  fromOffset: 'earliest' // equivalent of auto.offset.reset valid values are 'none', 'latest', 'earliest'
};


const consumer = new Kafka.ConsumerGroup(consumerOptions, ['sensor_data_1', 'sensor_data_2']);

const kafka = new Producer();

consumer.on('message', (metadata) => {

  // Output the actual message contents
  const dataValue = JSON.parse(metadata.value.toString());
  
  logger.info(`data value: ${metadata.value.toString()}`);

  const resData = {};

  resData[dataValue.timestamp] = dataValue.data;
  resData.id = dataValue.id; 

  if (dataValue.id === 'sensor_1') {
    kafka.sendMessage('sensor_data_filtered_1', resData);
  } else {
    kafka.sendMessage('sensor_data_filtered_2', resData);
  }

  

});

//logging all errors
consumer.on('error', (err) => {
  logger.error('Error from consumer ' + err +  'code: ' + err.code);
});
